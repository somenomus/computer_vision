'''
After initial imports define a 3x3 kernel and a 5x5 kernel, and then we load the
image in grayscale. After that  convolve the image with each of the kernels.
 several library functions available  . NumPy provides
the convolve function;  it only accepts one-dimensional arrays. Although the
convolution of multidimensional arrays can be achieved with NumPy,  
 SciPy's ndimage module provides another convolve function, which supports
multidimensional arrays. Finally, OpenCV provides a filter2D function (for convolution
with 2D arrays) and a sepFilter2D function (for the special case of a 2D kernel that can be
decomposed into two one-dimensional kernels). The preceding code sample illustrates the
ndimage.convolve function.

'''




import cv2
import numpy as np
from scipy import ndimage

kernel_3x3 = np.array([[-1, -1, -1],
                       [-1,  8, -1],
                       [-1, -1, -1]])

kernel_5x5 = np.array([[-1, -1, -1, -1, -1],
                       [-1,  1,  2,  1, -1],
                       [-1,  2,  4,  2, -1],
                       [-1,  1,  2,  1, -1],
                       [-1, -1, -1, -1, -1]])

img = cv2.imread("../images/vase2.jpg", 0)

k3 = ndimage.convolve(img, kernel_3x3)
k5 = ndimage.convolve(img, kernel_5x5)

blurred = cv2.GaussianBlur(img, (17,17), 0)
g_hpf = img - blurred

cv2.imshow("3x3", k3)
cv2.imshow("5x5", k5)
cv2.imshow("blurred", blurred)
cv2.imshow("g_hpf", g_hpf)
cv2.waitKey()
cv2.destroyAllWindows()
